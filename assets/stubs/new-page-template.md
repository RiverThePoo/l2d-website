---
title: "Long Page Title"
linkTitle: "Short Nav Title (typically the same as title for L2D pages)"
description: >-
     Page description for heading and indexes.
---

## Heading

Edit this template to create your new page.

* Give it a good name (where it says "change-me.md" above), ending in `.md` - e.g. `getting-started.md`.  Avoid multi-worded file names where possible and use hyphens for spaces when needed.
* Edit the "front matter" section at the top of the page (titles and description).
* Add a good commit message at the bottom of the page (<80 characters; use the extended description field for more detail).
* Follow proper Markdown format (https://www.markdownguide.org/cheat-sheet/).
* Create a new branch so you can preview your new file and request a review via Merge Request.
