---
title: "Release 2022.11.4"
linkTitle: "2022.11.4"
date: 2022-11-27
type: blog
robotsdisallow: true
exclude_search: true
---

## Changelog

- Dropped uniques now display their name
- Piglin and Piglin Brutes in the nether map no longer turn into Zombified Piglins
  - Piglin spawns in the nether map are caused by Zombified Piglin Spawners.  As such, most Piglins will still be zombified and only those that spawn naturally will not be zombified (which is rare due to the number of spawners).
- New unique added to Nether map: Cinnabun
- Mob spawn levels reduced slightly

