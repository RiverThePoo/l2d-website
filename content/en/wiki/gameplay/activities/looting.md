---
title: "Looting"
linkTitle: "Looting"
type: docs
description: >
  Looting and griefing the maps
---

Gathering materials is done primarily via looting and griefing of the [zombie worlds]({{< ref "/wiki/gameplay/worlds/zombieworlds" >}}).

## Looting

There are two types of loot chests on Left2Die: Static and Auto-loot.  Static chests will always contain the same loot while auto-loot chests will randomly generate new loot.

### Static

Static chests will always have the same items in them every time the maps [reset]({{< ref "/wiki/gameplay/restart" >}}).  Items like [uniques]({{< ref "/wiki/gameplay/activities/uniques" >}}) are typically found in static loot chests, however, other items such as enchanted books and other assorted loot items can be found in them as well.  These chests are first come, first served per reset.


### Auto-Loot

Auto-loot chests are randomly generated loot chests.  Their locations are always the same; only the loot changes.  Auto-loot chests are per-player, so every player can open the chest and get their own set of loot items.  They also give [coins]({{< ref "/wiki/gameplay/features/economy" >}}).  These chests will reset their loot every four hours regardless of when the maps [reset]({{< ref "/wiki/gameplay/restart" >}}).

There are three tiers of auto-loot chest each of three levels (so nine total types).  These are indicated when opening the chest in the chest GUI as `T1L1` and similar.  The chest locations will shuffle each time the maps reset, so the location of a `T3L3` chest one reset may be a `T1L1` the next reset.

## Griefing

To get building blocks, players will need to grief the zombie world maps.  The general rule is: if you can break it, it's yours.  The maps will [reset]({{< ref "/wiki/gameplay/restart" >}} every few hours.

{{% alert title="Warning" color="warning" %}}
This also applies to [Plotworld]({{< ref "/wiki/gameplay/worlds/plotworld" >}})!  Be careful on who you trust to add to your plot; griefing of plots will not be repaired!
{{% /alert %}}

