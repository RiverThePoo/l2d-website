---
title: "Heads"
linkTitle: "Heads"
type: docs
description: >
  Collect heads hidden on the server
---

This is a partial list of the player heads located within the maps. This is not a complete list. 

## Aquashock

- ninjasparten07

## Islands

- Jaysonbond
- ninjasparten07
- tnt378

## Megamap

- AmazingLemonz
- Br0ken0ut
- MightyMasterMax
- Moonfeather
- OhPistolPete
- Ralen
- toboein
- zenith4183

## City

- BlueCharm
- FatalTryHardz
- NightMelodia
- TheDeathBadger
- toboein
- zenith4183

## Town

- PuarZ
- toboein
- zenith4183
- zyxwa2000

## Nether

- twitchywitchy

## Satz

- EagleTheory
- hammy607
- JaysonBond
- KillerSkull
- ninjasparten07
- tnt378

## Tiny

- bkauf
- Bybloss
- EndyDaBlooper
- iminez1472
- JohnBPKC
- PuarZilla
- TheDeathBadger
- zenith4183

