---
title: "Tutorial"
linkTitle: "Tutorial"
type: docs
weight: 1
description: >
  This page will help you get started with L2D so you don't get too lost. ;)
---

## TL;DR

1. Read the [rules]({{< ref "/wiki/general/rules" >}}). We have a no ban appeal policy.
2. Grief and loot the [zombie worlds]({{< ref "/wiki/gameplay/worlds/zombieworlds" >}}) for supplies (`/town`, `/city`, `/themed`). Zombie worlds reset every four hours.
3. Melon is the only cure for zombie [infection]({{< ref "/wiki/gameplay/features/infection" >}}).
4. Claim a [plot]({{< ref "/wiki/gameplay/worlds/plotworld" >}}) (`/p auto`) in spawn (`/spawn`) to store loot at. Return to your plot with `/home`.
5. Kill zombies, vote, or sell in `/ah` or `/shop` for [money]({{< ref "/wiki/gameplay/features/economy" >}}).
6. Kill zombies to [rank]({{< ref "/wiki/gameplay/features/ranks" >}}) up.
7. Check out the rest of the wiki when you have time or just explore and discover things on your own.


## What is Minecraft Left2Die?

Minecraft Left2Die is a zombie apocalypse themed server for Minecraft. It's a survival server with a zombie twist. L2D consists of two types of world, the [zombie world]({{< ref "/wiki/gameplay/worlds/zombieworlds" >}}) and [plotworld]({{< ref "/wiki/gameplay/worlds/plotworld" >}}). Grief and loot the zombie world for supplies and build in the safety of plotworld.

When you first login to Left2Die, you will be in the [Town]({{< ref "/wiki/gameplay/worlds/zombieworlds/megamap/town" >}}) area which is the easier spawn point of the Megamap. The other spawn point in this world is the [City]({{< ref "/wiki/gameplay/worlds/zombieworlds/megamap/city" >}}). In addition to Megamap, there is also the [Themed]({{< ref "/wiki/gameplay/worlds/zombieworlds/themed" >}}) world which consists of a series of maps that rotate every four hours.

When you die, you will appear in Plotworld. The helicopters contain warp signs that can be used to go back to the zombie world. You can also use warp commands to get around (see below), however using the commands will cost coins.

## Rules

Please read the rules either here on the [rules]({{< ref "/wiki/general/rules" >}}) page or in-game with `/rules`. "I didn't know" is not a valid excuse, nor is "My brother/sister used my account".

## Getting Around

After your first run around in the zombie world, the next thing you should probably do is claim a plot. You must be in Plotworld (`/spawn`) first, then you can claim a plot with /p auto. This will cost 30 coins, but don't worry, you start with 500 coins and making money is fairly easy.

You can return to your plot with `/home`, however you must first be in plotworld (`/spawn` or `/pw`) to use the command.

Once you have your plot, you can return to the zombie world with `/spawn` or `/pw` and then use one of the map helicopters. You can also use the warp commands (`/city`, `/town`, `/themed`) to go directly to the zombie world spawn you wish to visit. Using warp signs and warping within plotworld is free, however if you use a warp command to warp between zombie world and plotworld, there will be a 20 coin charge.

## Getting Loot

Use `/kit` to get some starting tools and armour so you don't die straight away. While you are at the ZombieBait rank, you will automatically get this kit each time you die (with a cooldown). You will get better [kits]({{< ref "/wiki/gameplay/features/kits" >}}) by [ranking]({{< ref "/wiki/gameplay/features/ranks" >}}) up.

On L2D, if you can break it, it's yours. This includes plotworld, so don't give perms to your plot to people you can't trust completely.

The maps contain several loot chests and of course the builds. Grief it all you want! The zombie maps get reset every four hours.

You can view when the maps will next get reset with `/wr` (short for "when's restart").

## Making Money

You can earn [money]({{< ref "/wiki/gameplay/features/economy" >}}) by killing zombies and [voting]({{< ref "/wiki/contribute/vote" >}}).

You will need money for warping via command (warp signs are free).

## Infection

Zombies may infect you if you get hit by one. Infected players can also infect you.

Melon is the cure for the [infection]({{< ref "/wiki/gameplay/features/infection" >}}). It may take several melons to cure yourself depending on the level of infection. A bar will appear on your screen to indicate how infected you are.

If the infection goes untreated for too long, you will turn into a zombie. You must be cured by another player via glistening melon within two minutes or you will die. They must right click you with a single glistening melon while you have full health. While it may sound fun to become a zombie, you are very limited in ability, so it is not a state you want to get yourself into.

## Bandages

In your kit you'll find some [bandages]({{< ref "/wiki/gameplay/features/bandages" >}}).

Left click the air to heal yourself. Right click another player to heal them.

Bandages won't work if your health reaches two hearts or below.

## Ranking Up

You rank up by killing zombies. See the rank page for more details.

## Voting

If you like the server, please consider [voting]({{< ref "/wiki/contribute/vote" >}}). Type `/vote` in game and follow the links in the GUI. Voting will earn you rewards!

