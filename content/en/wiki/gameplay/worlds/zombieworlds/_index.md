---
title: "Zombie Worlds"
linkTitle: "Zombie Worlds"
type: docs
weight: 2
description: >
  The zombie, looting worlds
---

Minecraft Left2Die has several maps to play on. All zombie gameplay maps reset every four hours when the server restarts. You can view how long it is until the next restart with the command `/wr`.

The zombie worlds can be reached via `/town` and `/city` for the two spawn points in the Megamap or via `/themed` for the current themed map.

The link to the map [overviewer](https://map.minecraftleft2die.com/) can be obtained via the `/map` command. This also shows the location of all bosses and spawn points.

