---
title: "Titanic"
linkTitle: "Titanic"
type: docs
description: >
  The Titanic
---

Survive the zombie hoards on this historic ship.

{{< cardpane >}}
{{< card header="**Titanic**">}}

<center>{{< figure src="/img/wiki/Titanic.png" width="308px" height="334px" >}}</center>

**First Release:** March 7, 2015

**First Retirement:** January 6, 2018

**Second Release:** May 22, 2019

**Size:** 424x171

{{< /card >}}
{{< card header="**Builders**">}}

**Schematic Creator:**

- Unknown (link lost)

**L2D Team**

- zenith4183
- BlueCharm
- TheDeathBadger


{{< /card >}}
{{< /cardpane >}}

The Titanic map was created from an online schematic of the Titanic. The original link to the schematic was lost, so the original creator's name is currently unknown.

The schematic was only the ship itself. The backdrop, ocean, and sea floor were all created separately by zenith4183 while the iceberg was created by TheDeathBadger. BlueCharm setup most of the loot chests.

## Trivia

- The Frozen Jellyfish [unique]({{< ref "/wiki/gameplay/activities/uniques" >}}) was the result of zenith thinking the iceberg looked like a jellyfish as it was being built.
- Most of the other uniques were named to fit with the Titanic theme. For instance, the unique 2:20am was the time that the Titanic sunk.
The sign at the front of the ship is in reference to the scene from the movie Titanic. An image was staged with LaserLasagna at this point of the ship.
{{< figure src="/img/wiki/Titanic_scene.png" width="308px" height="334px" >}}
