---
title: "Zender"
linkTitle: "Zender"
type: docs
description: >
  Slenderman themed map
---

Players are stuck with perpetual blindness in this Slenderman based map.

{{< cardpane >}}
{{< card header="**Zender**">}}

<center>{{< figure src="/img/wiki/Zendermap.png" width="308px" height="334px" >}}</center>

**First Release:** November 7, 2015 

**First Retirement:** January 6, 2018

**Second Release:** May 22, 2019

**Size:** 400x400

{{< /card >}}
{{< card header="**Builders**">}}

**L2D Team**

- TheDeathBadger

{{< /card >}}
{{< /cardpane >}}

Zender is based on the Slenderman game. While in the map, players are completely blinded making it difficult to find ones way around and even more difficult to survive hoards.

The Zender map is the only map with a boss made specifically for it, the Zenderman, that can lurk anywhere throughout the entire map.

## Trivia

- The map was laid out to roughly match the layout of the Slenderman world.
- TheDeathBadger created the map entirely on his own, using schematics for some of the builds.
- The maps contain a series of seven pages for players to find.
