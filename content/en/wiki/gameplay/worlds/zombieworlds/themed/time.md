---
title: "Time"
linkTitle: "Time"
type: docs
description: >
  The largest themed map featuring a large clock
---

Our largest themed map features a large clock, hence the name. Expect to do a lot of climbing among the mountains. 

{{< cardpane >}}
{{< card header="**Time**">}}

**First Release:** July 4, 2016

**First Retirement:** May 22, 2019

**Second Release:** January 1, 2020

**Size:** 1120x1120

{{< /card >}}
{{< card header="**Builders**">}}

**Map Creator:**

- dennisbuilds

**L2D Team**

- zenith4183
- BlueCharm
- TheDeathBadger
- tnt378
- IrishDaPanda1259
- Hawkushin


{{< /card >}}
{{< /cardpane >}}


## Trivia

- The largest of the maps in the themed rotation.
- There are several sheep hidden in the map by tnt378. These were used as part of a contest when the map was released.
