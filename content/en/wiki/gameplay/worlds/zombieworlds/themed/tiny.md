---
title: "Tiny Terrors"
linkTitle: "Tiny Terrors"
type: docs
description: >
  A large house where you're tiny
---

The map isn't tiny, but you are. Tiny is a house scaled up to make you feel like a mouse. The map is still in progress.


{{< cardpane >}}
{{< card header="**Tiny Terrors**">}}

<center>{{< figure src="/img/wiki/Tiny_terrors.png" width="308px" height="334px" >}}</center>

**Floor 1 Release:** September 7, 2014

**Floor 2 Release:** December 27, 2015

**Attic Release:** December 2, 2017

**Size:** 575x249

{{< /card >}}
{{< card header="**Builders**">}}

**L2D Team**

- Bybloss
- JohnBPKC
- eggo123
- Jacki
- BlueCharm
- TheDeathBadger
- viravampira
- IrishTheRat
- Archer114897
- EndyDaBlooper
- MadamBunny

{{< /card >}}
{{< /cardpane >}}

Tiny was the third new map released on L2D at Minecats and the seventh over all. It depicts a large scale house to make the player appear tiny.

Tiny is the longest in progress map on L2D and has been in the works off and on since it was started in 2014. Currently only the inside of the house is released. 

## Trivia

- When the kitchen was being built, two builders built two different toasters on opposite sides of the counter not realizing it. Zenith found it funny and it became a joke to have toasters all over the map in odd locations.
- The outside of the house was accidentally made two different colours with none of the builders noticing it. Zenith later fixed it and the oddity went away when floor 2 was released.
- Just before Floor 2 was expected to be released, zenith was inspecting the map when he noticed something seemed off.  The entire build team had completely forgotten to add windows on the entire floor.  Several wall decorations had to be relocated to make room for windows.
- While statues were used to determine scale, some parts of the house clearly do not follow this scale.
- Tiny was originally released as its own map. It is now part of the themed map rotation.
- When originally released, the map had so many spawners that the server could not handle it. Builders had to clear out about half of the original spawners.
- Tiny contains a few easter eggs that only players that played L2D at its original location would understand.
- Several of the builds were built on the creative server and then later copied over to L2D.
- Due to its vast size and the amount of time it takes to do builds for Tiny, it has become more of a side project for builders to work on between working on other maps. There is no ETA on the final completion.
- The final build is expected to be three floors, a front and back yard, and a garage.
