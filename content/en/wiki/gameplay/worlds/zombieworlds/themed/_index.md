---
title: "Themed"
linkTitle: "Themed"
type: docs
description: >
  The rotating themed maps
---

The themed maps rotate every restart and are accessible via `/themed` or warp signs at the spawn points.

The currently active themed map can be viewed with the `/whatisthemed` command, via the hologram in front of the themed helicopter at [Plotworld]({{< ref "/wiki/gameplay/worlds/plotworld" >}}) spawn, or in the `#player-notifications` channel in [Discord](https://discord.gg/JWZXt8V).

