---
title: "Megamap"
linkTitle: "Megamap"
type: docs
description: >
  The primary zombie world
---

The Megamap is Left2Die's primary zombie and looting world.  It contains two spawn points located at `/town` and `/city`.

{{% alert title="Tip" color="info" %}}
The edge of the Megamap drops off into the void.  Don't run off!
{{% /alert %}}
