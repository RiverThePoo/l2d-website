---
title: "Plotworld"
linkTitle: "Plotworld"
weight: 2
type: docs
description: >
  The safe world
---

Plotworld is the safe area of L2D where you can build a house and store your loot.

Plotworld spawn is the location of the enchantment tables (you can of course also make your own).

<img src="/img/wiki/Plotworld.png"  width="300px" height="170px" />

## Commands

### Plot Claiming

| Command     | Description                                                                              | Cost       |
| ----------- | ---------------------------------------------------------------------------------------- | ---------- |
| /plot auto  | Claim the next available plot.                                                           | $30        |
| /plot claim | Claim a blank plot.                                                                      | $30        |
| /plot buy   | Buy a plot that is for sale. You cannot go over your plot number limit by buying a plot. | Sale Price |

### Warp Commands

| Command            | Description                                                                                                                           |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------------- |
| /pw                | Go to plotworld spawn                                                                                                                 |
| /plot sethome      | Sets your home point on your plot. This is where /p h will take you.                                                                  |
| /plot h            | Teleport you to your plot. This command can only be used while in plotworld.                                                          |
| /plot h #          | Teleport to a particular plot. This command can only be used while in plotworld. Only ranks with multiple plots can use this command. |
| /plot visit <name> | Teleport you to another player's plot if you are added to that plot. This command can only be used while in plotworld.                |

### Plot Permissions

{{% alert title="Warning" color="warning" %}}
Be careful who you add to your plot. Added players can grief and loot your chests. If they are added, then this is allowed per the rules.
{{% /alert %}}

| Command              | Description                                                                                                  |
| -------------------- | ------------------------------------------------------------------------------------------------------------ |
| /plot add <name>     | Add a player to your plot. They will be able to break blocks and access chests only when you are online.     |
| /plot trust <name>   | Allow a player access to your plot even when you are offline.                                                |
| /plot remove <name>  | Remove a player from your plot (reverse of /plot add and /plot deny).                                        |
| /plot untrust <name> | Remove player from trust status. They will still be able to access your plot when you are online.            |
| /plot deny <name>    | Deny a player access to your plot.                                                                           |
| /plot info           | Shows ownership and everyone added and denied from your plot or whatever plot you are currently standing on. |
| /plot list           | Lists every plot you have perms on.                                                                          |

### Plot Maintenance

| Command             | Description                                            | Required Rank |
| ------------------- | ------------------------------------------------------ | ------------- |
| /plot biome <biome> | Change the biome of your plot.                         | Infected      |
| /plot clear         | Delete everything on your plot. This cannot be undone. | Zombiebait    |
| /plot delete        | Clear and delete your plot. This cannot be undone.     | Zombiebait    |

### Plot Merging

Only ranks with multiple plots can merge plots. Once merged, all commands will act against the entire merged plot.

{{% alert title="Warning" color="warning" %}}
If you do /p clear or /p delete on a merged plot the ENTIRE MERGED PLOT will be wiped.
{{% /alert %}}

| Command                 | Description                                                                                                                                                                      | Cost |
| ----------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---- |
| /plot merge <direction> | Merge your plot to the plot in the direction you specify. If you do not own the plot, the owner will be sent an invite. If they accept, the merged plot will contain two owners. | $500 |
| /plot unlink            | Unmerges a merged plot. This unmerges everything and anything where the roads were will be destroyed. 	                                                                     |      |

### Plot Flags

| Command                          | Description                                                                                                       | Required Rank |
| -------------------------------- | ----------------------------------------------------------------------------------------------------------------- | ------------- |
| /plot protect<br>/plot unprotect | Protect and unprotect a plot from expiration.                                                                     | ZombieSlayer  |

## Plot Expiration

Plots expire after three months of no block activity (placing and breaking blocks, chest opening does NOT count) or logging in. ZombieSlayer rank and higher can protect their plots from deletion when they expire. Once reset, the plot cannot be restored.

## Limitations

Plotworld has the following limitations to prevent server lag and abuse:

- There is a limit of 10 animals per plot.
- There is a limit of 3 vehicles (boats, minecarts).
- There is a limit of 50 total entities (mobs + vehicles + item frames).
- Natural mob spawning is disabled. You will need to find mob eggs.
- Mobs that wander into the roads will die.
- Redstone will only be active so long as the plot is occupied. As soon as you leave the plot or log off, the redstone will turn off.
- Leaves will not decay.
- TNT is not enabled on plots.
