---
title: "Changelog"
linkTitle: "Changelog"
type: docs
weight: 2
description: >
  The latest changes made to the server
---

{{% alert title="Notice" color="info" %}}
New changelog updates have been moved to [here]({{< ref "/news/releases" >}}).  This page is kept for historical purposes.

Changelogs are also posted on our [Discord](https://discord.gg/JWZXt8V).
{{% /alert %}}

## 2022

July 4

- New website

July 2

- Texture pack re-added

## 2021

November 6

- Re-enabled villager spawn eggs in Plotworld
- Re-enabled enderpearls in Plotworld
- Removed Player Market (`/ah`)
- Removed Server Market (`/warp shop`)
- Changed server software:
  - May result in less kicks for some players with less stable internet connections to the server.
  - Should improve server performance
  - Fixed Ted/Giant AI
  - Nerfed villager AI

November 2

- Tiny Terrors map re-enabled.

September 29

- Tiny removed from themed rotation
- Boats and minecarts disabled in themed maps

September 26

- Scoreboard removed
- Scoreboard info now in tab menu
- Nighttime PvP removed

September 25

- Replaced EvilAnnouncer with a newer, eviler EvilAnnouncer
- Removed Clans

March 8

- New unique viewer with `/unique list`

January 15

- Zombies now break doors

## 2020

July 26

- Loot tables updated

July 25

- `/plot invite` removed
- `/home` is now free to use
- `/warp` commands now change when used from PlotWorld

March 27

- Fixed PvP status name tagging for 1.15
- Fixed /pvp toggle command
- Cut loot chest max coin amount back to previous lower value.
- Cut max balance to 1k coins to get coins from killing zombies.
- Increased PvP safety time for new players to max amount of 1000 minutes.
- Set themed maps back to PvP always enabled mode.
- Fixed anti-combat logger plugin for 1.15 (maybe)
- Fixed anti-auto-fisher for 1.15
- Added new perks to /vote shop (more coming later for donators)

Jaunary 1

- `/votethemed` command added for donators to vote for the themed map for the next restart

## 2019

May 22

- Zender, Satz, and Titanic themed maps brought out of retirement. End, Nether, and time retired.

March 23

- The server will now reset the maps and rotate the themed map without having to restart the server.
	- The server will still restart at midnight Eastern time.
	- The server will still restart if it detects that it is running into problems.
	- During a map reset, players will now be sent to Plotworld to wait until the world is reset.
- Items obtained via `/kit` are now labeled as [Kit Item].
	- Kit items can no longer be sold in the player market.
	- As a result of the new tag, kit items will not stack with non-kit items of the same type. Plan accordingly.
- Player data is now stored more frequently, so loss of zombie kill statistics should be minimal now in the event of a server crash.
- Various code cleanup and removal of no longer used features.

## 2018

December 15

- The dedicated PvP map has been removed. The Islands map will return later as one of the maps in the Themed rotation.
- PvP is now enabled globally in the zombie worlds at night. A warning message and sound will trigger about a minute before PvP starts and immediately after it starts. edit: PvP has been disabled in `/themed` at least for now. This may change again in the future.
- All players get 100 minutes of PvP safe time to start and PvP disabled. Each night, that time will tick down. If it reaches zero, PvP automatically becomes enabled for that player.
- Players that have PvP turned off with have a green name tag. Players that have it on will have a red name tag. Keep in mind that a player that has it off can turn it on!
- Players can enable/disable their PvP status with `/pvp enable` (or `/pvp on`) and `/pvp disable` (or `/pvp off`). Players can also use `/pvp toggle` to switch to the opposite of whatever mode they’re currently on. Disabling PvP costs 500 coins.
- The scoreboard on the right will display the total number of minutes remaining when PvP is disabled. When there are less than ten minutes left, it will turn red in colour.
- Players can purchase more PvP safetime via Fred at `/spawn` or with `/pvp buytime`. The cost is 5 coins per minute.
- The following things cannot be done during the night:
	- Disabling PvP (you can still enable though)
	- Purchasing more safe time.
	- Warping via command (except for in themed which is always night). Warp signs will still work. You will be warned when warping to a location where PvP is enabled and you have PvP enabled to allow you a chance to return to spawn and make changes.
- The underground PvP areas in the City map are no longer PvP.
- The amount of coins that can appear in loot chests has been tripled.

July 8

- Added taxes to be collected every Sunday following these rates:
	- 0-10K: No Taxes
	- 10k-100k: 1% per week
	- 100k-1M: 5% per week
	- \> 1M: 10% per week

March 31

- New Features
	- New recipe for the SpeedClock. A clock surrounded by sugar.
	- New player login messages are back.
- Changes
	- Finished the new warping code and replaced the former warp plugin.
	- Rewrote the displayed message code to allow for future localization.
	- Rewrote the ranking code.
	- Possible fix implemented in an attempt to keep the world times in sync with each other.
	- Renamed ZombieControl to ChatMod to make their job more clear to new players.

February 18

- The MegaMap now has a daylight cycle.
 	- Zombies do not burn in sunlight.
 	- Zombies will not spawn in open, sunny areas, but will still spawn indoors and in shaded areas.
- New `/help` command.
- Updated the colour scheme of `/rules`.
- Cleaned out some old permissions.

January 20

- All plugins updated to their 1.12 compatible versions.
- The old vote rewards plugin has been replaced with something 1.12 compatible.
	- Votes now award tokens which can be redeemed in the `/vote` shop.
	- A melon is awarded for every vote.
	- You can now see exactly how long you have to wait to vote for each voting site.
	- Use `/vote gui` to see all options.

January 6

- Maps
	- Closed Olympus due to lack of use and complications with the planned 1.12.2 upgrade.
	- Removed the following maps from the themed rotation due to lack of popularity:
		- Zender
		- SatZ
		- Titanic
- Plugins
	- Deprecated the lottery due to a lack of popularity and usage.
	- Deprecated all plugins relating to Olympus.
	- Deprecated the plugin used for custom login/leave messages due to 1.12.2 issues.
	- Cleaned out the permissions file of users with no assigned group (leftovers from when L2D had a voter rank).
	- Deprecated the modreq plugin. Use `/report` now to report players violating the rules.
- Chat
	- Removed unused chat channels: Trade (t), Help (h), and Olympus (o).
	- Troll (tr) channel renamed to Mature (ma).
- Bug Fixes
	- Fix some players not being sent to spawn after a restart (again…this time it should actually work).
	- Lots of code cleanup.


## 2017

December 2

- Attic of Tiny Terrors released.

Week of September 24

- Added a new bandage recipe.
- Started shrinking Plotworld.
- Fixed bug allowing flame arrows to damage players in non-PvP zones.
- Re-enabled the Minecraft tipped arrows.
- Added Minecraft tipped arrows and dragon's breath to the Armoury Crate.
- Deprecated world border plugin.

Week of September 17

- Replaced old player market with new plugin that is updated for 1.12.
- The server shop is back at `/shop`.
- Plotworld only commands will now say as such when attempting to use them in the zombie worlds.
- Brought the build server back online to continue map development.
- Upgraded several more plugins in preparation for the 1.12 upgrade.
- Warping fees are now deposited into the server account.
- Kit fees now get deposited into the server account.
- Deprecated info books for new player logins.
- Deprecated the special arrows plugin.
- Deprecated the nether portal control plugin.
- Removed the strict talk perms on TS.
- Made some changes to staff commands.
- Removed "No Infection" voting milestone.
- Started changing the rank colour scheme.

Week of September 10

- Increased the maximum balance a player can have to receive zombie kill money to 10k.
- Zombie kill milestone payments are now subject to the same maximum balance to be paid out.
- Zombie kill milestones are now fixed at 100 coins per 100 kills.
- New tutorial and rules signs at Town spawn (where new players spawn).
- Scoreboard now displays on the right hand side of the screen. Disable it with `/scoreboard`.
- Removed the weird glass building on the beach in Megamap.
- Removed the abnormally large airplane over the desert in Megamap.
- Updated the Tiny Terrors map to the latest version.
- Removed the 1.8 style PvP plugin. It wasn't being used anyways.
- Relocated the shop chests to a neighboring tent at spawn while a new shop is being setup.
- Pasted in some long gone plots back into plotworld to try and make the area look a little less flat.
- Removed `/money top`
- Removed the money payout for killing Zerg.
- Fixed some voting rewards that were not paying out money and giving XP.
- Partially fixed the issue of some players logging in at the wrong spot after a restart.
- Upgraded several plugins in preparations for the 1.12 upgrade.

June 11

- Minecraft Left2Die joins the Elemental Division Network.

May 27

- New players now receive their kit automatically upon first login. ZombieBait ranks will receive their kit automatically upon death, but only if they survive between deaths a certain amount of time.

## 2016

September 3

- Lotteries added. Click here for details.

September 2

- Eggs crate is finally working again and back at `/warp crates`.
- Join/leave messages changed.
- New players automatically receive a kit and a welcome message on first join.
- Global message sent when a player joins the server for the first time.
- Welcome message for all players.

August 31

- Warping out of the PvP map is re-enabled. Click here for details.

August 30

- Wiki relocates. New website.

August 19

- New limits added to the maximum number of zombie kills that will apply to a player's rank per restart.
- New limits on the amount of money made for killing zombies.

August 14

- New plotworld spawn.

June 12

- L2D relocates to the Atlantic Network.

May 23

- Lupen the Demon Wolf boss added to Megamap.
- Leveled zombies added. Higher levels have more health, speed, and pay more when killed. Higher levels may drop items.

January 23

- Required points for each rank have been cut in half.

January 22

- Dark Knight boss added to MegaMap.

## 2015

December 20

- Bacon boss updated.

December 18

- Sheep King re-released and updated.
- King Skele updated.

December 16

- Olympus made harder. Mobs have special abilities, blocks are harder to break, armour weighs players down, etc.

November 21

- Ranks are now effected by bandage usage, PvP kills, and voting. Deaths and grinding have a negative impact on ranking up.

November 7

- Zender map released as part of the theme rotation.

October 16

- Added voting crates.

October 3

- Megamap re-released.

September 27

- L2D database and database backups corrupted. Money, clans, kill stats, voting stats, and plot associations were lost. Plots are being moved to the new plotworld.
- Plots can now be merged by donators allowing for building in the road area.
- Some plot commands have changed. Notably, `/p h <name> #` and `/p trust`.

September 12

- The underground PvP and PvP field in Town no longer allow warping out of them.
- Enderpearls are disabled in PvP combat.
- Donator kits disabled.

September 6

- There are now three PvP spawn points. The dock is now PvP enabled.
- Players can no longer warp out of PvP and must return to a helicopter or exit via the PvP to City tunnel.

July 3

- Plots now go on sale after two months of no owner login or block activity. They expire after three months.

June 3

- Expired plots will now be auto deleted every restart.

May 30

- The server now restarts every four hours instead of every two. Olympus resets every eight hours now.
- `/spawn` is now free to use when in plotworld.

May 2

- Autospawn for Sir Clucksalot boss has been re-enabled. He now spawns in SatZ.

April 19

- Players will now spawn where they last logged off until the next restart.
- Infected players will no longer be able to teleport to plotworld.

April 12

- King Skele boss re-added to the Islands map.

April 7

- Draco boss added as an autospawn boss in the city map.

April 1

- Killer Rabbit of Caerbannog boss added to Olympus.

March 15

- New kits system added.

March 14

- Donators can now go to Olympus without 50 zombie kills.

March 7

- New plotworld spawn.
- `/spawn` moved to plotworld.
- Aquashock, Tiny, SatZ, Nether, and Titanic now rotate randomly each restart as `/themed`. Non-active maps will not be available while not at `/themed`.
- New Titanic map.
- Islands map is back as `/pvpspawn`.
- Improvements and new areas/loot added to Islands, City, and Town.
- Donators now get three plots for Infected and Four plots for Survivor.

February 1

- Olympus hardcore map added.

January 11

- Killing zombies while standing in the same spot will no longer count towards ranking up or mob killing rewards.

January 3

- Capture the Flag removed.

## 2014

November 14

- SatZ map added as the new `/pvpspawn`.
- Islands map retired.

November 11

- Being infected too long will result in player zombification.
- Bandages now have a five second cooldown.

September 21

- Jose the Happy Ghast boss added to the nether map.
- Zerg boss added to the Town map.

September 7

- Tiny Terrors map released at `/tiny`
- Ted the Giant boss added to tiny.

August 3

- Nether map released at `/nether`

July 5

- You can now rank up by killing zombies.

June 14

- Bacon boss re-added to town map.

May 31

- Aquashock map released at `/aquashock`
- Zim boss added to Aquashock map.

May 28

- Sheep King re-added to town map.

May 13

- KingSkele boss re-added to `/pvpspawn`.

May 11

- Islands map is back as `/pvpspawn`

May 6

- Capture the Flag added.

July 28

- KingSkele removed due to crashing issues.

June 4

- Town map readded as a separate map.

April 26

- L2D relocates to Minecats. City map is the only active map at this moment.

March 1

- Zombie infection has been updated. Infection now has multiple stages.
- Bandages have been re-added.

## 2013

December 27

- Special arrows added as a donator perk.

December 8

- Islands map from the first L2D added to the mega map. Made the new spawn.

November 9

- New mega map released incorporating both City and Town. Map is 2500x2500 in size.

July 29

- Town map released replacing the City map.

June 9

- L2D returns with the debut of the City map.

January 4

- L2D is shut down.

## 2012

July

- L2D Opens

