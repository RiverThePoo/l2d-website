---
title: "Economy"
linkTitle: "Economy"
type: docs
description: >
  Money and the server economy
---

Minecraft Left2Die uses Coins as its currency. Money can be used for using warp commands.

## Making Money

Making money is relatively easy and can be obtained by either killing zombies (for players with less than 10k) or Voting.

## Money Management

| Command                    | Description                               |
| -------------------------- | ----------------------------------------- |
| /money                     | View how much money you have.             |
| /money pay <name> <amount> | Pay an amount of money to another player. |
