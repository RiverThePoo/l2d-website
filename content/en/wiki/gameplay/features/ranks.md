---
title: "Ranks"
linkTitle: "Ranks"
type: docs
description: >
  Ranking up on the server
---

## Ranking Up

Ranking up is based upon a rank score determined by the following factors:

- Killing zombies (*1 point each*)
- Killing players in PvP (*3 points each*)
- Healing yourself with [bandages]({{< ref "/wiki/gameplay/features/bandages" >}}) (*1 point per 10 uses*)
- Healing others with bandages (*1 point per 5 uses*)
- Voting (*2 points*)
- Grinding zombies (*-1 point per 5*)
- Grinding players in PvP (*-5 points each*)
- Dying (*-5 points per death*)

You can view how many points you need to rank up with /rank.  See your individual statistics with /stats.

### Camping/Grinding

As noted above, grinding zombies and players can reduce your rank score. Follow these tips and you will not have this issue.

- Move! You are much more unlikely to lose points from grinding if you move around and not stay in the same spot.
- Do not kill the same player twice within the same hour.

## Ranks

### ZombieBait

- Points Required: 0
- Kits: [zombiebait]({{< ref "/wiki/gameplay/features/kits#zombiebait" >}})
- Chance of double damage dealt: 0%
- Chance of half damage received: 0%
- Speed Clock Boost Time: 5 seconds
- Default Rank

### Recruit

- Points Required: 1,000
- Kits: [recruit]({{< ref "/wiki/gameplay/features/kits#recruit" >}})
- Chance of double damage dealt: 2%
- Chance of half damage received: 1%
- Speed Clock Boost Time: 6 seconds

### Scout

- Points Required: 5,000
- Kits: [scout]({{< ref "/wiki/gameplay/features/kits#scout" >}})
- Chance of double damage dealt: 4%
- Chance of half damage received: 2%
- Speed Clock Boost Time: 7 seconds
- TNT Allowed

### Medic

- Points Required: 25,000
- Kits: [medic]({{< ref "/wiki/gameplay/features/kits#medic" >}})
- Chance of double damage dealt: 6%
- Chance of half damage received: 3%
- Speed Clock Boost Time: 8 seconds
- TNT Allowed

### ZombieSlayer

- Points Required: 50,000
- Plots: 2
- Plot Protection: (`/p protect`)
- Kits: [zombieslayer]({{< ref "/wiki/gameplay/features/kits#zombieslayer" >}})
- Chance of double damage dealt: 8%
- Chance of half damage received: 4%
- Speed Clock Boost Time: 9 seconds
- TNT Allowed

### Infected

- Points Required: 250,000
- Kits: [infected]({{< ref "/wiki/gameplay/features/kits#infected" >}})
- Chance of double damage dealt: 10%
- Chance of half damage received: 5%
- Speed Clock Boost Time: 10 seconds
- Plots: 3
- Plot Protection (`/p protect`)
- TP perms (plotworld only)
- `/me` Perms
- Plot Teleports (without invites)
- Infected Chat Channel
- No Cooldowns
- Free Warps
- TNT Allowed

### Survivor

- Points Required: 500,000
- Kits: [survivor]({{< ref "/wiki/gameplay/features/kits#survivor" >}})
- Chance of double damage dealt: 12%
- Chance of half damage received: 6%
- Speed Clock Boost Time: 11 seconds
- Plots: 4
- Toggle Infection
- All Infected Perks

