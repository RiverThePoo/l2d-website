---
title: "Kits"
linkTitle: "Kits"
type: docs
description: >
  Rank kits for basic gear
---

Each rank comes with a kit to assist players in L2D.  You can get your kit with /kit.  Higher ranks can change to a lower default kit with the command `/kit <lower rank>`.  Changing your kit has a $100 fee per change. View all kits available to you with `/kit list`.

## Cooldowns and Costs

| Rank         | Cooldown (minutes) | Cost |
| ------------ |:------------------:|:----:|
| ZombieBait   | 15                 | 0    |
| Recruit      | 30                 | 50   |
| Scout        | 45                 | 100  |
| Medic        | 60                 | 200  |
| ZombieSlayer | 75                 | 300  |
| Infected     | 90                 | 400  |
| Survivor     | 105                | 500  |

## Kit Contents

### ZombieBait

- Iron Sword
- Iron Helmet
- Iron Chestplate
- Iron Leggings
- Iron Boots
- 10 Bread
- 5 Cooked Pork
- 5 [Bandages]({{< ref "/wiki/gameplay/features/bandages" >}})
- 64 Oak Planks

### Recruit

- Bow
- 32 Arrows
- Diamond Helmet
- Iron Chestplate
- Iron Leggings
- 8 Cooked Chicken
- 64 Spruce Planks

### Scout

- Iron Axe
- Diamond Helmet
- Iron Chestplate
- Iron Leggings
- Iron Boots
- 2 Enderpearls
- [Speedclock]({{< ref "/wiki/gameplay/features/speedclock" >}})
- 8 Cooked Chicken
- 64 Birch Planks

### Medic

- Gold Helmet
- Diamond Chestplate
- Leather Leggings
- Diamond Boots
- Golden Apple
- 10 Melon
- Glistening Melon
- 10 Paper
- 10 Red Dye
- 8 Cooked Beef
- 64 Jungle Planks

### Zombieslayer

- Diamond Sword
- Leather Helmet
- Chainmail Chestplate
- Diamond Leggings
- Diamond Boots
- 2 Gold Apples
- Gold Hoe
- 8 Cooked Fish
- 64 Acacia Planks

### Infected

- Diamond Sword
- Bow
- 20 Arrows
- Iron Helmet
- Iron Chestplate
- Diamond Leggings
- Diamond Boots
- Enchanted Golden Apple
- 15 Melon
- 8 Cooked Mutton
- 64 Dark Oak Planks

### Survivor

- Diamond Sword
- Bow
- 32 Arrows
- Diamond Chestplate
- Diamond Leggings
- Diamond Boots
- Enchanted Golden Apple
- 20 Melon
- 8 Cooked Rabbit
- 64 Quartz Blocks

