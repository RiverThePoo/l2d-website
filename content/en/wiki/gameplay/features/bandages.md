---
title: "Bandages"
linkTitle: "Bandages"
type: docs
description: >
  Bandages can be used to heal yourself and other players
---

<img src="/img/wiki/Bandage_recipe.png"  width="158px" height="184px" />

Bandages can be used to heal yourself or another player.  Making a bandage requires two paper, one white wool, and one pink dye following the recipe shown on the right.  To heal yourself, left click the bandage.  To heal another player, right click them with the bandage. Using a bandage applies 6 HP (three hearts).

Using bandages comes with a five second cooldown.  They also cannot be used to heal oneself once the player's health is at two hearts or below.

Bandage usage also effects a player's [rank]({{< ref "/wiki/gameplay/features/ranks" >}}).
