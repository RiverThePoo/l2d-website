---
title: "Disabled Features"
linkTitle: "Disabled Features"
type: docs
description: >
  Vanilla features disabled on the server
---

This page contains a list of most of the disabled features on Left2Die. This is not a complete list, but contains what players typically ask about. 


## Globally
### Game Mechanics

* Firespread
* Leaf decay

### Items

* Elytra
* Firecharges (lower than scout rank)
* Flint and Steel (lower than scout rank)
* Phantom Membrane
* Slow Falling Potions
* Totem of Undying


### Mobs

* Villagers


## Zombie Worlds
### Game Mechanics

* Spawner breaking

### Items

* Armour Stands
* Beacons
* Conduit
* Coral
* Coral Fans
* Chorus fruit
* Dragon Eggs
* Lava buckets
* Piston
* Shulker boxes
* Sticky Piston
* TNT (lower than scout rank)
* TNT minecarts (lower than scout rank)
* Trapdoors

### Mobs

* Most passives

## Plotworld
### Items

* Enderpearls
* TNT (everyone)
* TNT minecarts (everyone)

### Mobs

* All hostiles
* Iron Golems
