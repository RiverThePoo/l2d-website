---
title: "Minecraft Left2Die Wiki"
linkTitle: "Wiki"
type: docs
weight: 20
menu:
  main:
    weight: 20
---


## What is Minecraft Left2Die?

Minecraft Left2Die is a zombie apocalypse themed minecraft survival server (not a mini-game). Loot and grief the city for supplies to build fresh in the new world. Be careful though. Zombies are everywhere and can infect you with the zombie plague.

Join us at play.minecraftleft2die.com!

Read the [Tutorial](/wiki/gameplay/tutorial) page to get the basics of Minecraft Left2Die. 

